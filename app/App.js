import React, { Component } from "react";
import AppDrawerNavigator from "./component/AppDrawerNavigator";
import Note from "./bean/Note";

export default class App extends Component {
	render() {
		return <AppDrawerNavigator />;
	}
}
